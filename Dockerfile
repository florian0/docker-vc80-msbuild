FROM registry.gitlab.com/florian0/docker-wine32

ENV WINEARCH=win32
ENV WINEPREFIX=/root/.wine


RUN export DISPLAY=:0 && \
        nohup bash -c "Xvfb :0 -screen 0 1024x768x16 &" && \
        sleep 2 && \
        wineboot && \
        wget -q https://cmake.org/files/v3.12/cmake-3.12.4-win32-x86.msi && \
        wine msiexec /i cmake-3.12.4-win32-x86.msi /q && \
        wine reg add HKCU\\Environment /v PATH /d "C:\Program Files\CMake\bin" /f && \
        rm cmake-3.12.4-win32-x86.msi && \
        winetricks -q dotnet20 vc2005trial 2> /dev/null && \
        rm -rf ~/.cache/winetricks
